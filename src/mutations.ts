import { MessageData, BulkMessagesPayload } from '@/types'
import { MutationTree } from 'vuex'

export const mutations: MutationTree<BulkMessagesPayload> = {
    newMessage(state, payload: MessageData) {
        state.messages[state.messages.key()] = payload
    }
}