import { MessageState, Message } from './types';
import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { RootState } from '../types';

export const state: MessageState = {
	Messages: new Map<string, Message>(),
};

const namespaced: boolean = true;

export const messages: Module<MessageState, RootState> = {
	namespaced,
	state,
	getters,
	actions,
	mutations,
};
