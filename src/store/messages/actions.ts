import { ActionTree } from 'vuex';
import { MessageState, Message } from './types';
import { RootState } from '../types';

export const actions: ActionTree<MessageState, RootState> = {
	pushMessage({ commit }, payload: Message): any {
		commit('addMessage', payload);
	},
}
