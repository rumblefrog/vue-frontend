import Vue from 'vue';
import Vuex from 'vuex';

import { RootState } from './types';

import { messages } from './messages';

Vue.use(Vuex);

export default new Vuex.Store<RootState>({
	state: {
		version: '0.0.someting.dev.version',
	},
	modules: {
		messages,
	},
	mutations: {

	},
	actions: {

	},
})
